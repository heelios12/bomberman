using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// INHERITANCE
public class BlockDestructibleReceivingExplosion : ControllerReceivingExplosion
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void ReceivingExplosion()
    {
        Destroy(gameObject);
    }
}
