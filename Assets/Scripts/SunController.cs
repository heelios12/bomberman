using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunController : MonoBehaviour
{
    public static int rotation;
    public static int numberLight;
    // Start is called before the first frame update
    void Start()
    {
        ChangeLight();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeLight()
    {
        transform.eulerAngles = new Vector3( rotation, 0, 0);
    }

}
