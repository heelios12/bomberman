using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;

public class ExplosionManager : MonoBehaviour
{
    [SerializeField] GameObject explosionPrefab;
    int finishedExplosions  = 0;

    private Vector3 sizeExplosionUnit = new Vector3(0.2f, 0.2f, 0.2f);
    // Start is called before the first frame update
    void Start()
    {
         Instantiate(explosionPrefab, transform.position, explosionPrefab.transform.rotation);
         StartCoroutine(ExplosionCoroutine(Vector3.forward));
         StartCoroutine(ExplosionCoroutine(Vector3.back));
         StartCoroutine(ExplosionCoroutine(Vector3.right));
         StartCoroutine(ExplosionCoroutine(Vector3.left));
    }

    // Update is called once per frame
    void Update()
    {
        if(finishedExplosions == 4)
            Destroy(gameObject);
    }

    IEnumerator ExplosionCoroutine(Vector3 direction)
    {
        bool done = false;
        int cantMove = 1;
        if(!IsCollidingWithAIndestructibleBlock(transform.position + direction * cantMove))
        while(!done)
        {
            done = IsCollidingWithADestructibleBlock(transform.position + direction * (cantMove)) ||
            IsCollidingWithAIndestructibleBlock(transform.position + direction * (cantMove + 1));
            Instantiate(explosionPrefab, transform.position + direction * cantMove, explosionPrefab.transform.rotation);
            cantMove++;
            yield return new WaitForSeconds(0.5f);
        }
        finishedExplosions++;
    }

    bool IsCollidingWithADestructibleBlock(Vector3 position) => Physics.OverlapBox(position, sizeExplosionUnit)
            .Any(c => c.CompareTag("DestructibleBlock"));
    bool IsCollidingWithAIndestructibleBlock(Vector3 position) => Physics.OverlapBox(position, sizeExplosionUnit)
            .Any(c => c.CompareTag("IndestructibleBlock"));
}
