using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionMenuEfect : MonoBehaviour
{
    [SerializeField] float timeExplosion;
    [SerializeField] GameObject explosionEfect;
    [SerializeField] AudioClip audioClip;
    [SerializeField] float audioVolume;
    AudioSource audioSourceBomb;
    GameObject confetti;
    // Start is called before the first frame update
    void Start()
    {
        audioSourceBomb = GetComponent<AudioSource>();
        Invoke("Blow", timeExplosion);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void Blow()
    {
        confetti = Instantiate(explosionEfect, transform.position, explosionEfect.transform.rotation);
        audioSourceBomb.PlayOneShot(audioClip, audioVolume);
        GetComponent<MeshRenderer>().enabled = false;
        Invoke("DestroyBombAndConfettis",5f);
    }

    void DestroyBombAndConfettis()
    {
        Destroy(confetti);
        Destroy(gameObject);
    }
}
