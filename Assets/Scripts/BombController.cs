using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombController : MonoBehaviour
{

    BoxCollider bombCollider;
    [SerializeField] float timeExplosion;
    [SerializeField] GameObject generateExplosion;
    
    // Start is called before the first frame update
    void Start()
    {
        bombCollider = GetComponent<BoxCollider>();
        Invoke("Blow", timeExplosion);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerExit(Collider other)
    {
        if(other.gameObject.name == "Player")
            bombCollider.isTrigger = false;
    }

    void Blow()
    {
        Instantiate(generateExplosion, transform.position, generateExplosion.transform.rotation);
        Destroy(gameObject);
    }
}
