using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// INHERITANCE
public class PlayerReceivingExplosion : ControllerReceivingExplosion
{
    [SerializeField] GameObject menuButton;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void ReceivingExplosion()
    {
        Debug.Log("El pinguino murio :(");
        Destroy(gameObject);
        menuButton.SetActive(true);
    }
}
