using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using JetBrains.Annotations;

public class Explosion : MonoBehaviour
{
    bool damageActive = true;
    [SerializeField]AudioClip[] explosionClips;
    AudioSource explosionAudio;
    // Start is called before the first frame update
    void Start()
    {
        explosionAudio = GetComponent<AudioSource>();
        explosionAudio.PlayOneShot(explosionClips[Random.Range(0, explosionClips.Length)], 1.0f);
        Invoke("DesactiveDamage", 0.1f);
        Invoke("DestroyThisGameobject", 1.5f);
    }

    // Update is called once per frame
    void Update()
    {
        if(damageActive)
            foreach (var collider in Physics.OverlapBox(transform.position, new Vector3(0.5f, 0.5f, 0.5f)))
            {
                var gameobjectReceivingExplosion = collider.gameObject.GetComponent<ControllerReceivingExplosion>();
                // POLYMORPHISM
                if(gameobjectReceivingExplosion != null)
                    gameobjectReceivingExplosion.ReceivingExplosion();
            }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawCube(transform.position, new Vector3(0.2f, 0.2f, 0.2f));
    }

    void DestroyThisGameobject()
    {
        Destroy(gameObject);
    }
    void DesactiveDamage()
    {
        damageActive = false;
    }


    
}
