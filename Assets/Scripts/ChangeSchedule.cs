using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ChangeSchedule : MonoBehaviour
{
    [SerializeField] int[] rotationsLight;
    [SerializeField] string[] timeOfDay;
    [SerializeField] SunController sun;
    [SerializeField] TextMeshProUGUI text;
    // Start is called before the first frame update
    void Start()
    {
        UpdateLight();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Change()
    {
        // ABSTRACTION
        IncrementLight();
        UpdateLight();

    }

    void IncrementLight()
    {
        SunController.numberLight++;
        if(SunController.numberLight == timeOfDay.Length)
            SunController.numberLight = 0;
    }

    void UpdateLight()
    {
        text.text = timeOfDay[SunController.numberLight];
        SunController.rotation = rotationsLight[SunController.numberLight];
        sun.ChangeLight();
    }
}
