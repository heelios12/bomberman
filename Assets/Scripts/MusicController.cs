using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour
{
    [SerializeField] AudioClip[] audios;
    AudioSource thisAudioSource;
    // Start is called before the first frame update
    void Start()
    {
        thisAudioSource = GetComponent<AudioSource>();
        thisAudioSource.clip = audios[SunController.numberLight];
        thisAudioSource.Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
