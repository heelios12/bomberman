using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Unity.VisualScripting.Dependencies.Sqlite;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // ENCAPSULATION
    [SerializeField] float speed;
    [SerializeField] GameObject bombPrefab;
    [SerializeField] GameObject pinguin;

    [SerializeField] AudioClip placeBombSound;
    AudioSource playerSound;
    // Start is called before the first frame update
    void Start()
    {
        playerSound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
            PlaceBomb();
    }

    void FixedUpdate()
    {
        Movement();
        Rotation();
    }

    void Movement()
    {
        transform.Translate(Vector3.right * Input.GetAxis("Horizontal") * speed * Time.deltaTime, Space.World);
        transform.Translate(Vector3.forward * Input.GetAxis("Vertical") * speed * Time.deltaTime, Space.World);
    }

    void Rotation()
    {
        if(Input.GetKey(KeyCode.W))
            pinguin.transform.eulerAngles = new Vector3(-89.98f, 0, pinguin.transform.rotation.z);
        else if(Input.GetKey(KeyCode.S))
            pinguin.transform.eulerAngles = new Vector3(-89.98f, 180, pinguin.transform.rotation.z);
        else if(Input.GetKey(KeyCode.D))
            pinguin.transform.eulerAngles = new Vector3(-89.98f, 90, pinguin.transform.rotation.z);
        else if(Input.GetKey(KeyCode.A))
            pinguin.transform.eulerAngles = new Vector3(-89.98f, -90, pinguin.transform.rotation.z);
    }

    void PlaceBomb()
    {
        playerSound.PlayOneShot(placeBombSound, 1.0f);
        Instantiate(bombPrefab, GetPositionBomb(), bombPrefab.transform.rotation);
    }

    Vector3 GetPositionBomb() => new Vector3(Mathf.Round(transform.position.x), 0, Mathf.Round(transform.position.z));
}
